FROM python:3.12-alpine

# Install extra system packages.
RUN apk add libpq

# Install build dependencies temporarily such that we can install Python dependencies.
RUN apk add --no-cache --virtual .build-deps gcc postgresql-dev musl-dev python3-dev libpq-dev

# Install Python dependencies via pip and requirements_freeze.txt.
# Dependencies are installed with --no-deps which ensures that
# requirements_freeze.txt has a complete set of all dependencies.
COPY requirements_freeze.txt /tmp/
RUN pip install --no-deps -r /tmp/requirements_freeze.txt

# Build dependencies are not longer needed.
RUN apk del --no-cache .build-deps

RUN mkdir -p /src
COPY src/ /src/
RUN pip install -e /src
COPY tests/ /tests/

WORKDIR /src
