# archpy

Exercise project for "Architecture Patterns with Python" book.

# Run Docker container

```
$ docker-compose run api
```

## Run tests

CI run, aka start docker and run all the tests.
```
$ docker-compose run tests
```

For incremental development+test faster to keep docker shell open and
run the tests there:

```
$ docker-compose run tests sh
...
$ pytest /tests
```

## Deps update

First, determine the update for the deps, say, upgrade SQLAlchemy.

1. Create a new virtualenv: `virtualenv ve && source ve/bin/activate`
2. Install current frozen deps: `pip install --no-deps -r requirements_freeze.txt`
3. Update the deps: `pip install -U SQLAlchemy`
4. Regen the frozen requirements: `pip freeze > requirements_freeze.txt`
5. Rerun tests: `docker-compose run --build tests`
6. Delete the virtualenv: `rm -rf ve`
7. Update `requirements.in` with the updated version.

It is the best to regen transitive dependencies after major version changes, to make sure
that (a) the `requirements.in` still produce non-conflicting environment (b) the environment doesn't
carry dependencies that are no longer necessary.

### Deps regen

Sometimes direct dependencies don't change, but the transitive do. To regen transitive dependencies:

1. Create a new virtualenv: `virtualenv ve && source ve/bin/activate`
2. Install current logical deps: `pip install -r requirements.in`
3. Regen the frozen requirements: `pip freeze > requirements_freeze.txt`
4. Rerun tests: `docker-compose run --build tests`
5. Delete the virtualenv: `rm -rf ve`

