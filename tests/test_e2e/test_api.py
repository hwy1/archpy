import uuid

import pytest
import requests

from allocator import config


def random_suffix():
    return uuid.uuid4().hex[:6]


def random_sku(name=""):
    return f"sku-{name}-{random_suffix()}"


def random_batchref(name=""):
    return f"batch-{name}-{random_suffix()}"


def random_order_id(name=""):
    return f"order-{name}-{random_suffix()}"


@pytest.mark.usefixtures("restart_api")
def test_happy_path_returns_201_and_allocated_batch(add_stock):
    sku, other_sku = random_sku("one"), random_sku("two")
    early_batch = random_batchref("early")
    later_batch = random_batchref("later")
    other_batch = random_batchref("other")
    add_stock(
        [
            (later_batch, sku, 100, "2011-01-02"),
            (early_batch, sku, 100, "2011-01-01"),
            (other_batch, other_sku, 100, None),
        ]
    )
    data = {"order_id": random_order_id(), "sku": sku, "qty": 3}
    url = config.get_api_url()

    r = requests.post(f"{url}/allocate", json=data)

    assert r.status_code == 201, f"Err: status={r.status_code} body={r.text}"
    assert r.json()["batchref"] == early_batch


@pytest.mark.usefixtures("restart_api", "db_session")
def test_unhappy_path_returns_400_and_error_message():
    unknown_sku, order_id = random_sku(), random_order_id()
    data = {"order_id": order_id, "sku": unknown_sku, "qty": 20}
    url = config.get_api_url()

    r = requests.post(f"{url}/allocate", json=data)

    assert r.status_code == 400, f"Err: status={r.status_code} body={r.text}"
    assert r.json()["message"] == f"Invalid sku {unknown_sku}"
