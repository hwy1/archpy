import contextlib
import os
import pathlib
import time

import pytest
import requests
import requests.exceptions
import sqlalchemy as sa
from sqlalchemy import orm as sa_orm
import sqlalchemy.exc

from allocator import config
from allocator.adapters import orm


@pytest.fixture(scope="session")
def db_connection():
    engine = sa.create_engine(config.get_db_uri())
    return wait_for_postgres_to_come_up(engine)


@pytest.fixture(scope="session")
def setup_db(db_connection):
    orm.start_mappers()
    yield


@pytest.fixture
def db_session(setup_db, db_connection):
    with maybe_begin(db_connection):
        orm.metadata.create_all(db_connection)

    yield sa_orm.scoped_session(sa_orm.sessionmaker(bind=db_connection))

    with maybe_begin(db_connection):
        orm.metadata.drop_all(db_connection)


def wait_for_webapp_to_come_up():
    deadline = time.time() + 10
    url = config.get_api_url()
    while time.time() < deadline:
        try:
            return requests.get(url)
        except requests.exceptions.ConnectionError:
            time.sleep(0.5)
    pytest.fail("API never came up")


@contextlib.contextmanager
def maybe_begin(connection):
    # https://docs.sqlalchemy.org/en/14/core/connections.html#migrating-from-the-nesting-pattern
    if not connection.in_transaction():
        with connection.begin():
            yield connection
    else:
        yield connection


def wait_for_postgres_to_come_up(engine):
    deadline = time.time() + 10
    while time.time() < deadline:
        try:
            return engine.connect()
        except sqlalchemy.exc.OperationalError:
            time.sleep(0.5)
    pytest.fail("Postgres never came up")


@pytest.fixture
def restart_api():
    app_dir = pathlib.Path(config.__file__).parent
    (app_dir / "entrypoints" / "web.py").touch()
    time.sleep(0.5)
    wait_for_webapp_to_come_up()


@pytest.fixture
def add_stock(db_session):
    batches_added = set()
    skus_added = set()

    # Fixture/helper which is used in tests.
    def _add_stock(lines):
        tx = db_session.begin()
        for ref, sku, qty, eta in lines:
            db_session.execute(
                sa.text(
                    "INSERT INTO batches (reference, sku, _purchased_quantity, eta) "
                    "VALUES (:ref, :sku, :qty, :eta)"
                ),
                {"ref": ref, "sku": sku, "qty": qty, "eta": eta},
            )
            [[batch_id]] = db_session.execute(
                sa.text("SELECT id FROM batches WHERE reference=:ref and sku=:sku"),
                {"ref": ref, "sku": sku},
            )
            batches_added.add(batch_id)
            skus_added.add(sku)
        tx.commit()

    yield _add_stock

    for batch_id in batches_added:
        db_session.execute(
            sa.text("DELETE FROM allocations WHERE batch_id=:batch_id"),
            {"batch_id": batch_id},
        )
        db_session.execute(
            sa.text("DELETE FROM batches WHERE id=:batch_id"), {"batch_id": batch_id}
        )
    for sku in skus_added:
        db_session.execute(
            sa.text("DELETE FROM order_lines WHERE sku=:sku"), {"sku": sku}
        )
    db_session.commit()
