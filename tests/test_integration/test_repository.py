import pytest
import sqlalchemy as sa

from allocator.adapters import repository
from allocator.domain import model


@pytest.fixture(params=(repository.SqlAlchemyRepository, repository.SqlRepository))
def repo_implementation(request):
    return request.param


def insert_order_line(db_session):
    db_session.execute(
        sa.text(
            "INSERT INTO order_lines (order_id, sku, qty) "
            "VALUES ('order1', 'GENERIC-SOFA', 12)"
        )
    )
    [[orderline_id]] = db_session.execute(
        sa.text("SELECT id FROM order_lines WHERE order_id=:order AND sku=:sku"),
        {"order": "order1", "sku": "GENERIC-SOFA"},
    )
    return orderline_id


def insert_batch(db_session, batch_reference):
    db_session.execute(
        sa.text(
            "INSERT INTO batches (reference, sku, _purchased_quantity, eta) "
            "VALUES (:ref, 'GENERIC-SOFA', 100, null)"
        ),
        {"ref": batch_reference},
    )
    [[batch_id]] = db_session.execute(
        sa.text("SELECT id FROM batches WHERE reference=:batch and sku=:sku"),
        {"batch": batch_reference, "sku": "GENERIC-SOFA"},
    )
    return batch_id


def insert_allocation(db_session, orderline_id, batch_id):
    db_session.execute(
        sa.text(
            "INSERT INTO allocations (orderline_id, batch_id) "
            "VALUES (:orderline_id, :batch_id)"
        ),
        {"orderline_id": orderline_id, "batch_id": batch_id},
    )


def test_repository_can_save_a_batch(db_session, repo_implementation):
    batch = model.Batch("batch1", "RUSTY-SOAPDISH", 100, eta=None)

    repo = repo_implementation(db_session)
    repo.add(batch)
    db_session.commit()

    rows = list(
        db_session.execute(
            sa.text("SELECT reference, sku, _purchased_quantity, eta FROM batches")
        )
    )
    assert rows == [("batch1", "RUSTY-SOAPDISH", 100, None)]


def test_repository_can_retrieve_a_batch_with_allocations(
    db_session, repo_implementation
):
    orderline_id = insert_order_line(db_session)
    allocated_batch_id = insert_batch(db_session, "allocated-batch")
    insert_batch(db_session, "non-allocated-batch")
    insert_allocation(db_session, orderline_id, allocated_batch_id)

    repo = repo_implementation(db_session)
    retrieved = repo.get("allocated-batch")

    expected = model.Batch("allocated-batch", "GENERIC-SOFA", 100, eta=None)
    assert retrieved.reference == expected.reference
    assert retrieved.sku == expected.sku
    assert retrieved._purchased_quantity == expected._purchased_quantity
    assert retrieved._allocations == ({model.OrderLine("order1", "GENERIC-SOFA", 12)})


def test_repository_lists_all_batches(db_session, repo_implementation):
    insert_batch(db_session, "batch-1")
    insert_batch(db_session, "batch-2")

    repo = repo_implementation(db_session)
    retrieved = repo.list()

    assert sorted([r.reference for r in retrieved]) == ["batch-1", "batch-2"]
