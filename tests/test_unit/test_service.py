import datetime

import pytest

from allocator.adapters import repository
from allocator.domain import model
from allocator.service import service


TODAY = datetime.date.today()
LATER = datetime.date.today() + datetime.timedelta(5)


class FakeRepository(repository.AbstractRepository):
    def __init__(self, batches):
        self._batches = batches

    def add(self, batch):
        self._batches.add(batch)

    def get(self, reference):
        return next(b for b in self._batches if b.reference == reference)

    def list(self):
        return list(self._batches)


class FakeSession:
    commited = False

    def commit(self):
        self.commited = True

    def rollback(self):
        self.commited = False


def test_returns_allocation():
    line = model.OrderLine("o1", "COMPLICATED-LAMP", 10)
    batch = model.Batch("b1", "COMPLICATED-LAMP", 100, eta=None)
    repo = FakeRepository([batch])

    batchref = service.allocate(line, repo, FakeSession())

    assert batchref == "b1"


def test_error_for_invalid_sku():
    line = model.OrderLine("ox", "NONEXISTENT", 10)
    batch = model.Batch("by", "REAL", 100, eta=None)
    repo = FakeRepository([batch])

    with pytest.raises(service.InvalidSkuException, match="Invalid sku NONEXISTENT"):
        service.allocate(line, repo, FakeSession())


def test_commits():
    line = model.OrderLine("oa", "OMINOUS-MIRROR", 10)
    batch = model.Batch("b1", "OMINOUS-MIRROR", 100, eta=None)
    repo = FakeRepository([batch])
    session = FakeSession()

    service.allocate(line, repo, session)

    assert session.commited is True


def test_allocations_are_persisted():
    sku = "FANCY-MIRROR"
    batch1 = model.Batch("b1", sku, 10, eta=TODAY)
    batch2 = model.Batch("b2", sku, 10, eta=LATER)
    line1 = model.OrderLine("o1", sku, 10)
    line2 = model.OrderLine("o1", sku, 10)
    repo = FakeRepository([batch2, batch1])
    session = FakeSession()

    # First order uses up all stock in batch1.
    batchref1 = service.allocate(line1, repo, session)
    # Second order should go to batch2.
    batchref2 = service.allocate(line2, repo, session)

    assert batchref1 == batch1.reference
    assert batchref2 == batch2.reference
