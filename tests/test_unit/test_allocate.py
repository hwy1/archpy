"""
Tests for Domain Service operations.

Domain Service represents a business process or a concept. In this case, it is
an allocations of order lines against a specific set of batches that represent
our stock.
"""

import datetime

import pytest

from allocator.domain.model import allocate
from allocator.domain.model import Batch
from allocator.domain.model import OrderLine
from allocator.domain.model import OutOfStockException


TODAY = datetime.date.today()
TOMORROW = TODAY + datetime.timedelta(1)
LATER = TODAY + datetime.timedelta(5)


def test_prefers_current_stock_batches_to_shipments():
    shipment_batch = Batch("in-stock-batch", "RETRO-CLOCK", 100, eta=TOMORROW)
    in_stock_batch = Batch("in-stock-batch", "RETRO-CLOCK", 100, eta=None)
    line = OrderLine("oref", "RETRO-CLOCK", 10)

    allocate(line, [shipment_batch, in_stock_batch])

    assert shipment_batch.available_quantity == 100
    assert in_stock_batch.available_quantity == 90


def test_prefers_earlier_batches():
    earliest = Batch("speedy-batch", "MINIMALIST-SPOON", 100, eta=TODAY)
    medium = Batch("normal-batch", "MINIMALIST-SPOON", 100, eta=TOMORROW)
    latest = Batch("slow-batch", "MINIMALIST-SPOON", 100, eta=LATER)
    line = OrderLine("test-order", "MINIMALIST-SPOON", 10)

    allocate(line, [latest, medium, earliest])

    assert earliest.available_quantity == 90
    assert medium.available_quantity == 100
    assert latest.available_quantity == 100


def test_returns_allocated_batch_ref():
    in_stock_batch = Batch("in-stock-ref", "GLOSS-POSTER", 100, eta=None)
    shipment_batch = Batch("shipment-ref", "GLOSS-POSTER", 100, eta=TOMORROW)
    line = OrderLine("oref", "GLOSS-POSTER", 10)

    allocation = allocate(line, [in_stock_batch, shipment_batch])

    assert allocation == in_stock_batch.reference


def test_raises_out_of_stock_exception_if_cannot_allocate():
    batch = Batch("batch-one", "SMALL-FORK", 10, eta=TODAY)

    allocate(OrderLine("order-1", "SMALL-FORK", 10), [batch])
    with pytest.raises(OutOfStockException, match="SMALL-FORK"):
        allocate(OrderLine("order-2", "SMALL-FORK", 1), [batch])
