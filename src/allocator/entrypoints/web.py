import flask
import sqlalchemy as sa
from sqlalchemy import orm as sa_orm

from allocator import config
from allocator.adapters import orm
from allocator.adapters import repository
from allocator.domain import model
from allocator.service import service


orm.start_mappers()
session_factory = sa_orm.sessionmaker(bind=sa.create_engine(config.get_db_uri()))


app = flask.Flask(__name__)


@app.route("/")
def index():
    return "Run and run", 200


@app.route("/allocate", methods=["POST"])
def allocate_endpoint():
    session = session_factory()
    repo = repository.SqlAlchemyRepository(session)
    line = model.OrderLine(
        flask.request.json["order_id"],
        flask.request.json["sku"],
        flask.request.json["qty"],
    )
    try:
        batchref = service.allocate(line, repo, session)
    except (model.OutOfStockException, service.InvalidSkuException) as err:
        session.rollback()
        return flask.jsonify({"message": str(err)}), 400
    except Exception as err:
        session.rollback()
        return flask.jsonify({"message": str(err)}), 500
    else:
        return flask.jsonify({"batchref": batchref}), 201
