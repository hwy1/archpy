from typing import Protocol, Iterator

import sqlalchemy as sa

from allocator.domain import model


class AbstractRepository(Protocol):
    def add(self, batch: model.Batch):
        raise NotImplementedError

    def get(self, reference: str) -> model.Batch:
        raise NotImplementedError


class SqlAlchemyRepository(AbstractRepository):
    """Repository based on SQLAlchemy ORM."""

    def __init__(self, session):
        self.session = session

    def add(self, batch: model.Batch):
        self.session.add(batch)

    def get(self, reference: str) -> model.Batch:
        return self.session.query(model.Batch).filter_by(reference=reference).one()

    def list(self) -> Iterator[model.Batch]:
        return self.session.query(model.Batch).all()


class SqlRepository(AbstractRepository):
    """Repository based on SQLAlchemy without ORM, using raw SQL."""

    def __init__(self, session):
        self.session = session

    def add(self, batch: model.Batch):
        self.session.execute(
            sa.text(
                "INSERT INTO batches (reference, sku, _purchased_quantity, eta) "
                "VALUES (:ref, :sku, :qty, :eta)"
            ),
            {
                "ref": batch.reference,
                "sku": batch.sku,
                "qty": batch._purchased_quantity,
                "eta": batch.eta,
            },
        )

    def get(self, reference: str) -> model.Batch:
        [[batch_id, reference, sku, qty, eta]] = self.session.execute(
            sa.text(
                "SELECT id, reference, sku, _purchased_quantity, eta FROM batches "
                "WHERE reference=:ref"
            ),
            {"ref": reference},
        )
        batch = model.Batch(reference, sku, qty, eta)
        order_lines = self.session.execute(
            sa.text(
                "SELECT ol.order_id, ol.sku, ol.qty "
                "FROM order_lines AS ol "
                "JOIN allocations AS a ON a.orderline_id = ol.id "
                "WHERE batch_id=:batch_id"
            ),
            {"batch_id": batch_id},
        )
        for order_id, sku, qty in order_lines:
            batch._allocations.add(model.OrderLine(order_id, sku, qty))
        return batch

    def list(self) -> Iterator[model.Batch]:
        references = self.session.execute(sa.text("SELECT reference FROM batches"))
        for [reference] in references:
            yield self.get(reference)
