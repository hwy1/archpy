"""
SQLAlchemy ORM mapper between persistent layer and domain model.
"""

import sqlalchemy as sa
from sqlalchemy import orm

from allocator.domain import model

registry = orm.registry()
metadata = sa.MetaData()


order_lines = sa.Table(
    "order_lines",
    metadata,
    sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
    sa.Column("sku", sa.String(255)),
    sa.Column("qty", sa.Integer, nullable=False),
    sa.Column("order_id", sa.String(255)),
)


batches = sa.Table(
    "batches",
    metadata,
    sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
    sa.Column("reference", sa.String(255)),
    sa.Column("sku", sa.String(255)),
    sa.Column("_purchased_quantity", sa.Integer, nullable=False),
    sa.Column("eta", sa.Date, nullable=True),
)


allocations = sa.Table(
    "allocations",
    metadata,
    sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
    sa.Column("orderline_id", sa.ForeignKey("order_lines.id")),
    sa.Column("batch_id", sa.ForeignKey("batches.id")),
)


def start_mappers():
    lines_mapper = registry.map_imperatively(model.OrderLine, order_lines)
    batches_mapper = registry.map_imperatively(
        model.Batch,
        batches,
        properties={
            "_allocations": orm.relationship(
                lines_mapper, secondary=allocations, collection_class=set
            ),
        },
    )
