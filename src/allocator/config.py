import os


def get_db_uri():
    return "postgresql://{user}:{pwd}@{host}/{db}".format(
        user=os.environ.get("DB_USER"),
        pwd=os.environ.get("DB_PASSWORD"),
        host=os.environ.get("DB_HOST"),
        db=os.environ.get("DB_NAME"),
    )


def get_api_url():
    host = os.environ.get("API_HOST", "127.0.0.1")
    port = 5005 if host == "localhost" else 80
    return f"http://{host}:{port}"
